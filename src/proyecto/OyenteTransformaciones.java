package proyecto;
import java.awt.Color;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import modelos.Poligono;
import modelos.Punto;
import modelos.Puntos;
import modelos.Transformaciones;
public class OyenteTransformaciones implements ActionListener {

    private PanelTransformaciones panel;
    private Transformaciones trans;

    public OyenteTransformaciones(Transformaciones trans, PanelTransformaciones panel) {
        this.panel = panel;
        this.trans = trans;

    }
    @Override
    public void actionPerformed(ActionEvent ae) {
        JButton origen = (JButton) ae.getSource();
        switch (origen.getName()) {
            case "invertir":
                int xC = 400;
                int yC = 300;
                trans.aplicarSimetriaCentral(xC, yC);
                break;
            case "translacion":
                trans.aplicarTranslacion(300, 100);
                break;
            case "rotacion":
                trans.aplicarRotacion(30);
                break;
            case "escalado":
                trans.aplicarEscalado(2);
                break;
            case "cambiar":
                String figura = panel.getOpcion();
                if (figura.equals("Triangulo")) {
                    int x = 200;
                    int y = 170;
                    int[] pX = {x, x + 50, x + 100};
                    int[] pY = {y, y - 100, y};
                    Puntos puntos = new Puntos();
                    for (int i = 0; i < pX.length; i++) {
                        puntos.add(new Punto(pX[i], pY[i], 2, Color.RED));
                    }
                    trans.setOriginal(new Poligono(puntos, Color.RED));
                    trans.setHomologa(null);
                } else if (figura.equals("Cuadrado")) {
                    int x = 200;
                    int y = 170;
                    int[] pX = {x, x + 100, x + 100, x, x};
                    int[] pY = {y, y, y + 100, y + 100, y};
                    Puntos puntos = new Puntos();
                    for (int i = 0; i < pX.length; i++) {
                        puntos.add(new Punto(pX[i], pY[i], 2, Color.RED));
                    }
                    trans.setOriginal(new Poligono(puntos, Color.RED));
                    trans.setHomologa(null);
                } else if (figura.equals("Pentagono")) {
                    int x = 200;
                    int y = 170;
                    int[] pX = {210, 175, 119, 119, 175, 210};
                    int[] pY = {180, 227, 209, 150, 132, 180};
                    Puntos puntos = new Puntos();
                    for (int i = 0; i < pX.length; i++) {
                        puntos.add(new Punto(pX[i], pY[i], 2, Color.RED));
                    }
                    trans.setOriginal(new Poligono(puntos, Color.RED));
                    trans.setHomologa(null);
                }
                break;
        }
        
        panel.repaint();
    }
}
