package proyecto;

import javax.swing.JFrame;
import modelos.Transformaciones;

public class Proyecto {
    public static void main(String[] args) {
        JFrame f = new JFrame("Transformaciones");
        Transformaciones trans = new Transformaciones();
        PanelTransformaciones panel = new PanelTransformaciones(trans);
        OyenteTransformaciones oyente = new OyenteTransformaciones(trans, panel);
        panel.addEventos(oyente);
        f.setSize(800, 700);
        f.setLocation(50, 50);
        f.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }
}
