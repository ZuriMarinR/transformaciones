/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import java.awt.*;

/**
 *
 * @author rafael
 */
public interface Dibujable {

  public void dibujar(Graphics g);

}
