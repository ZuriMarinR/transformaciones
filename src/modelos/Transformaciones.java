package modelos;

import java.awt.Color;
import java.awt.geom.Line2D;

public class Transformaciones {

    public static final int SIMETRIA_CENTRAL = 1;
    public static final int TRASLACION = 2;
    public static final int ROTACION = 3;
    public static final int HOMOTECIA = 4;
    protected Poligono original;
    private Poligono homologa;
    protected Line2D ejeSimetria;
    protected int tipoTransformacion;

    public Transformaciones() {
        crearShapeOriginal();
    }

    public void crearShapeOriginal() {
        int x = 200;
        int y = 170;
        int[] pX = {x, x + 50, x + 100};
        int[] pY = {y, y - 100, y};
        Puntos puntos = new Puntos();
        for (int i = 0; i < pX.length; i++) {
            puntos.add(new Punto(pX[i], pY[i], 2, Color.RED));
        }
        original = new Poligono(puntos, Color.RED);
    }

    public void setOriginal(Poligono poligono) {
        original = poligono;
    }

    public Poligono getOriginal() {
        return original;
    }

    public Poligono getHomologa() {
        return homologa;
    }

    public Line2D getEjeSimetria() {
        return ejeSimetria;
    }

    public int getTipoTransformacion() {
        return tipoTransformacion;
    }

    public void aplicarSimetriaCentral(int xC, int yC) {
        Puntos puntos = new Puntos();
        for (Punto punto : original.getPuntos()) {
            int xH = 2 * xC - punto.x;
            int yH = 2 * yC - punto.y;
            puntos.add(new Punto(xH, yH, 2, Color.RED));
        }
        setHomologa(new Poligono(puntos, Color.CYAN));
        tipoTransformacion = SIMETRIA_CENTRAL;
    }

    public void aplicarTranslacion(double dX, double dY) {
        Puntos puntos = new Puntos();
        for (Punto punto : original.getPuntos()) {
            int xO = punto.x;
            int yO = punto.y;
            int xH = (int) (xO + dX);
            puntos.add(new Punto(xH, yO, 2, Color.RED));
        }
        setHomologa(new Poligono(puntos, Color.CYAN));

        tipoTransformacion = TRASLACION;
    }

    public void aplicarRotacion(double angulo) {
        double rad = Math.toRadians(angulo);
        Puntos puntos = new Puntos();
        for (Punto punto : original.getPuntos()) {
            int xO = punto.x;
            int yO = punto.y;
            int xH = (int) (xO * Math.cos(rad) - yO * Math.sin(rad));
            int yH = (int) (xO * Math.sin(rad) + yO * Math.cos(rad));
            puntos.add(new Punto(xH, yH, 2, Color.RED));
        }
        setHomologa(new Poligono(puntos, Color.CYAN));
        tipoTransformacion = ROTACION;
    }

    public void aplicarEscalado(float factor) {
        Puntos puntos = new Puntos();
        for (Punto punto : original.getPuntos()) {
            int xO = punto.x;
            int yO = punto.y;
            int xH = (int) (xO * factor);
            int yH = (int) (yO * factor);
            puntos.add(new Punto(xH, yH, 2, Color.RED));
        }
        setHomologa(new Poligono(puntos, Color.CYAN));
        tipoTransformacion = HOMOTECIA;
    }

    /**
     * @param homologa the homologa to set
     */
    public void setHomologa(Poligono homologa) {
        this.homologa = homologa;
    }
}
