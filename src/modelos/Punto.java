package modelos;

import java.awt.*;

public class Punto extends Point implements Dibujable {

  private int radio;
  private Color color;

  public Punto(int x, int y, int radio, Color color) {
    super(x, y);
    this.radio = radio;
    this.color = color;
  }

  public Punto() {
    this(0, 0, 0, Color.BLACK);
  }

  @Override
  public void dibujar(Graphics g) {
    g.setColor(color);
    g.fillOval(x - radio, y - radio, 2 * radio, 2 * radio);
    g.setColor(Color.BLACK);
    g.drawOval(x - radio, y - radio, 2 * radio, 2 * radio);
  }
}
